#ifndef INCLUDED_HELPER
#define INCLUDED_HELPER
#include <string>
#include <iostream>
#include "kBucket.h"
#include "triple.h"
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
using namespace std;




//PRE:arrray is a character array
//POST:takes the number specified in chars in the array and makes it into an int
int charToInt(char array[]);


//PRE:toParse is a string that we will split up by the delimiter that
//is defined in the constants file, numArgs is the number of arguments
//that we find in the string toParse
//POST:returns an array of strings and numArgs by reference
void parser(string toParse,string parseArray[], uint & numArgs);

//PRE: takes a given time, and the about of seconds to be waited
//POST: will find the differance between given_time and the currently time
//      and will the differance is greater than the wait time it will return
//      true if not return false
//      need:
//        #include <time.h>
//      example:
//      time_t now;
//      time(&now);  /* get current time; same as: now = time(NULL)  */
//      while(timer(now, 5)) {
//        cout << "in while" << endl;
//      }
bool timer(time_t & given_time, double wait_time);

//PRE:myID is the id of your node, ip is a string representing an ip
//address, sockfd is the socket to send a message on
//POST:sends a message to the ipaddress specificed on the socket
//specified by sockfd and waits a certain amount of time, if it
//recieves a message back it returns true and exits, otherwise returns
//false
bool ping(uint myID, string ip, int & sockfd);

//PRE:takes two sockaddr_in objects, PORT is the port you want to make
//it on 
//POST:sets the sin_family and sin_port of the addresses 
void setSockaddr(sockaddr_in & sendAddress, sockaddr_in & recieveAddress, uint port);

//PRE:myID is your nodeID, message is the string you recievd, ip is
//the ipAddress that you recieved the 
//message from, sockfd is your socket, recieveAddress and sendAddress
//are variables you need to send and recieve, myKBucket is your
//kBucket that will need to alter
//POST:depending on what is held in the message the function calls a
//different function
void handleMessage(uint myID, string message, string ip, string myIP, int & sockfd, sockaddr_in sendAddress, sockaddr_in recieveAddress, kBucket & myKBucket, bool & exit, LinkedList <int> * myKeys);

// PRE:  Takes a socket, this nodes id, the node id that is trying to be found
//       and this nodes k buckets
//
// POST: Returns a pointer to the node closest to targetID
void initiateFindNode(int & sockfd, uint myID, uint targetID, kBucket & mykBucket,string myIP, triple & ans);

// PRE: Takes the socket identifier the ip adress that requested the
//      find node, the ID of this node, the ID of the targetNode and
//      the kBuckets of this node
//
// POST: A message has been sent to the senderIP containing the
//       kClosest nodes to this node
void handleFindNode(int & sockfd, string senderIP,
		    uint myID, uint targetID, kBucket & mykBucket,string myIP);

// PRE:  Takes a string to store response, the socket identifier
//       and an sockaddr_in to store the receiveAddress
//
// POST: Response contains the message sent from address
//       receiveAdress and recieveAdress has the adress info
int recieveMessage(string & response, int & sockfd, sockaddr_in & receiveAddress);

//PRE:takes a string
//POST:returns an int representing the string input
uint stringToInt(string aString);

// PRE:  Takes a nodeID, a sockaddr_in containing the adress
//       for nodeID, and the kBucket for the current node
//
// POST: insertTriple is called on myKBucket for a node created
//       using nodeID and receiveAddress
void refreshNode(int nodeID, string ipAddress, kBucket & myKBucket, int & sockfd);

// PRE:  Takes the current kClosest and numK is the number of nodes in
//       kClosest currently, takes the number of arguments in command and
//       the location of the read pointer along with an array of all
//       the commands containing alternating ipadress and nodeid values
//       for the new kClosest, takes the nodeID for the target, the array
//       of ints denoting what has been traversed, the number of k
//       contacted and responded
//
// POST: kClosest is updated to contain the closest out of kClosest and the new
//       nodes provided by command, traversed kContacted and kResponsed are
//       updated accordingly if any nodes in kClosest that had been contacted
//       were removed, if kClosest had empty slots numK is increased to the
//       number of nodes now contained within kClosest
void compareClosest(triple kClosest [], uint & numK, uint numArgs, uint & argsRead,
		    string command [], uint targetID, uint traversed [],
		    uint & kContacted, uint & kResponded, string myIP);

// PRE:  Takes the current kClosest nodes and number of nodes in
//       kClosest, Takes the number of nodes contacted, and the
//       array denoting which nodes have been traversed along with
//       the amount of currently pending requests, finally this function
//       takes the message to send and the socket indentifier
//
// POST: The first untraversed node in kClosest has been contacted with
//       message and aPending, kContacted, and traversed are updated accordingly
//       If there were no untraversed nodes nothing happens
void contactNewNode(triple kClosest [], int numK, uint & kContacted,
		    uint traversed [], uint & aPending, string message,
		    int & sockfd);

// PRE:  Takes the current kClosest nodes and the number of nodes in
//       kClosest, takes the number of nodes that have responded it
//       along with the array deonting which nodes have been
//       traversed, finally this takes the id of the node that
//       most responded to this node
//
// POST: traversed and kResponded are updated accordingly if
//       responseID is still contained within kClosest otherwise
//       nothing happens
void updateClosestVar(triple kClosest [], uint numK,
		      uint & kResponded, uint traversed [],
		      uint responseID);

// PRE:  Takes the current kClosest nodes and the number of nodes in
//       kClosest, also takes the nodeID to search for
//
// POST: if responseID is contained in kClosest the location of said
//       responseID in kClosest is returned, otherwise -1 is returned
uint findInClosest (triple kClosest [], uint numK, uint responseID);

// PRE:  Takes the current kClosest nodes and the number of nodes in
//       kClosest also takes the targetID
//
// POST: if numK > 0 returns a pointer to the triple in kClosest that
//       is closest to targetID, otherwise returns NULL
triple getAns(triple kClosest [], uint numK, uint targetID);

// PRE:  Takes an IP, a list of triple, and the number of triples
// POST: returns true if currIP is not in kClosest
bool checkInClosest(string currIP, triple kClosest [], uint numK);
#endif
