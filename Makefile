


node: main.o helper.o kBucket.o triple.o
	g++ main.o helper.o kBucket.o triple.o -o node

main.o: main.cc helper.h kBucket.h triple.h LinkedList.h Constants.h
	g++ -c main.cc

helper.o: helper.cc helper.h kBucket.h LinkedList.h triple.h Constants.h
	g++ -c helper.cc

kBucket.o: kBucket.cc kBucket.h LinkedList.h triple.h Constants.h
	g++ -c kBucket.cc

triple.o: triple.cc triple.h Constants.h
	g++ -c triple.cc









test-file: test-file.o
	g++ test-file.o -o test-log

test-file.o: test-file.cc

clean:
	rm *.o *~ test-log
