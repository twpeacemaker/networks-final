#ifndef INCLUDED_TRIPLE
#define INCLUDED_TRIPLE
#include <string>
#include <iostream>
#include "Constants.h"
using namespace std;

//CLASS CI: ipaddr is a defined string that exists as an IP
//address somewhere, port is a int that represents the port number of
//the ip address we are contacting for kademlia, nodeID represents the
//specific node that the IP address and port number are representing
//in kademlia

class triple{
 private:
  string ipaddr; //represents the ip address
  uint nodeID; //represents the nodeID of the node
  uint port; //represents the port number

 public:
  //Constructor
  triple();

  //constructor
  //PRE:none
  //POST:creates an object with ipaddr=pIpaddr, nodeID=pNodeID and
  //port=pPort
  triple(string pIpaddr, uint pNodeID, uint pPort);

  //Copy Constructor
  //PRE:Takes a triple as a parameter
  //POST:makes this object a deep copy of aTriple
  triple(const triple & aTriple);

  //PRE:all member data is defined
  //POST:returns the data in the triple as a string
  string getTriple() const;

  //PRE:all member data is defined
  //POST:returns the ip address of the triple
  string getIP() const;

  //PRE:all member data is defined
  //POST:returns the port
  uint getPort() const;

  //PRE:all member data is defined
  //POST:returns the nodeID
  uint getNodeID() const;

  // PRE:  Takes a message
  // POST: Sends message to node associated with this triple
  void sendMessage(string message, int & sockfd);

  //PRE:both objects satisfy the CI
  //POST:makes a deep copy of explicit equal to the implicit
  triple & operator = (const triple & pTriple);
    
  //PRE:takes a triple
  //POST:returns true if given triple equals this triple and false otherwise
  bool operator == (const triple & aTriple) const;

  
};
#endif
