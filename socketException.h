#ifndef INCLUDED_EXCEPTION
#define INCLUDED_EXCEPTION
#include <string>
#include <iostream>
using namespace std;

class socketException{
 private:
  string error;
 public:

  //PRE:constructor takes a string
  //POST:constructs the object
  socketException(string aException){
    error = aException;
  };

  //destructor
  ~socketException(){};

  //PRE:none
  //POST:reads the error message
  string description(){
    return error;
  };
};
#endif
