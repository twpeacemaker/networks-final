#include <sys/types.h>//fork
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string>
#include <string.h>
#include <iostream>
#include "LinkedList.h"
#include "Constants.h"
#include "kBucket.h"
#include "helper.h"
#include "triple.h"
#include "socketException.h"
#include <unistd.h>//fork
using namespace std;

int main(int argv, char * argc[]){
  if(argv == CORRECTNUMARGS){
    //ASSERT:we have the correct number of arguments on the command line
    uint myID = charToInt(argc[MYID]);
    uint arg5 = charToInt(argc[ARG5]);
    uint friendID = charToInt(argc[FRIENDID]);
    string myIP = argc[MYIP];
    string friendIP = argc[FRIENDIP];
    int valid = 0;

    

    uint pid = fork();
    if(pid == 0){
      //ASSERT:we are the child process
     
      //create socket here
      sockaddr_in sendAddress;
      sockaddr_in recieveAddress;
      sockaddr_in bindAddress;
      memset(&bindAddress, 0, sizeof(bindAddress));
      bindAddress.sin_port = htons(FORKPORT);
      bindAddress.sin_family = AF_INET;
      bindAddress.sin_addr.s_addr = INADDR_ANY;
      memset(&sendAddress, 0, sizeof(sendAddress));
      memset(&recieveAddress, 0, sizeof(recieveAddress));
      setSockaddr(sendAddress,recieveAddress,FORKPORT);
      recieveAddress.sin_port = htons(MAINPORT);
      inet_pton(AF_INET,myIP.c_str(),&sendAddress.sin_addr);
      int on = 1;
      valid = 0;
      int sockfd = socket(AF_INET,SOCK_DGRAM | SOCK_NONBLOCK,0);
      bind(sockfd, (struct sockaddr *) &bindAddress,
	   sizeof(bindAddress));
      LinkedList <int> * myKeys = new LinkedList <int>;

      //initializing kBucket and giving it our ID so we can use it in functions
      kBucket myKBucket(myID);
      
      if(arg5 == 0){
	//this is not the first node in the network so contact friend

	//We can assume that the kbuckets are empty so add triple
	triple friendTriple(friendIP,friendID,FORKPORT);
	myKBucket.insertTriple(friendTriple, sockfd);
	//ASSERT:our friends triple has been inserted at the correct level
	cout << "Enter find node" << endl;
	triple closest;
	initiateFindNode(sockfd, myID, myID, myKBucket,myIP, closest);
	cout << "Exiting find node" << endl;
      }

      bool exit = false;
      while(!exit){
	bool recieved = false;
	try{

	  //attempt to read message from socket
	  char buf[MAXRECV + 1];
	  memset(buf, 0, sizeof(buf));
	  //valid = recvfrom(sockfd,buf, MAXRECV,0,(struct sockaddr *) &recieveAddress,&sizeOfAddr);
	  //just need a variable made for sizeOfAddr, never use it
	  socklen_t sizeOfAddr = sizeof(recieveAddress);
	  valid = recvfrom(sockfd,buf,MAXRECV,0,(struct sockaddr *) &recieveAddress,
			   (socklen_t*) &sizeOfAddr);
	  if(valid == -1){
	    valid = 0;
	    throw socketException("Hello Shende");
	  }
	  //if it gets past this we have a message in buf
	  string data = buf;
	  cout << "*" << data << "*" << endl;
	  //get ip from message
	  //char ipBuf[IPLEN];
	  //memset(ipBuf, 0, sizeof(ipBuf));
	  //inet_ntop(AF_INET, &recieveAddress.sin_addr, ipBuf,IPLEN);
	  //ASSERT:ipBuf holds the ipaddress
	  //string ipAddress = ipBuf;
	  //cout << "Recieved from: " << ipAddress << endl;
	  uint numArgs;
	  string aArray[MAXWORDS];
	  parser(data,aArray,numArgs);
	  string messageID = aArray[0];
	  string ipAddress = aArray[1];
	  char array[BITS];
	  strcpy(array,messageID.c_str());
	  //ASSERT:messageArray[0] always holds the id of the person who sent
	  //the msg
	  uint nodeID = charToInt(array);
	  cout << "Entering handleMessage" << endl;
	  handleMessage(myID,data,ipAddress,myIP,sockfd,sendAddress,
			recieveAddress,myKBucket,exit,myKeys);
	  if(ipAddress != myIP){
	    //ASSERT:we recieved this message from another node so
	    //insert into kbucket
	    refreshNode(nodeID,ipAddress,myKBucket,sockfd);
	  }
	
	}catch(socketException e){};
      
      
      }
      //ASSERT:exit is true
      cout<<"GoodBye from Fork"<<endl;
      delete myKeys;
      
    }

    else{    
      //ASSERT:we are the parent process
      sockaddr_in sendAddress;
      sockaddr_in recieveAddress;
      setSockaddr(sendAddress,recieveAddress, MAINPORT);
      //set options for sendAddress because they never change except IP
      sendAddress.sin_port = htons(FORKPORT);
      //set this to forkPort cause we are only ever sending to fork

      int sockfd = socket(AF_INET,SOCK_DGRAM | SOCK_NONBLOCK,0);
      inet_pton(AF_INET,myIP.c_str(),&recieveAddress.sin_addr);
      bind(sockfd, (struct sockaddr *) &recieveAddress,
	   sizeof(recieveAddress));
      
      //Now both parent and child will be sending and recieving on
      //different ports as to not affect each other and both know what
      //ports to send to each other on.
      
      bool done = false;
      while(!done){
	cin.clear();
	string command = "";
	
	cout << "Enter Command: ";
	char aBuf[BUFSIZE];
	cin.getline(aBuf,MAXRECV);
	command = aBuf;
	
	uint numArgs = 0;
	string commandArray[MAXWORDS];
	parser(command,commandArray,numArgs);
	//ASSERT:we have all the commands parsed from the command line in commandArray

	string sendCommand= "";
	//cmd, parameters, nodeId)
	bool foundCommand = false;
	string stringID = argc[MYID];
	
	//**********************************************************************************
	//we should put this in a function after we have the socket stuff done so it looks nice,
	//we will just have to pass the socket by refernce in so we can use it as well as the
	//command array and the numArgs and we will have to pass the boolean 'done' in  as well
	
	if(commandArray[0] == STORE){
	  cout<<"here :"<<numArgs<<endl;
	  if(numArgs == NUMSTORE){
	    //store stuff here
	    sendCommand = stringID + " " + myIP + " " + commandArray[0] + " " + commandArray[1];
	    foundCommand = true;
	  }
	  else{
	    cout<<"There is an incorrect number of arguments, store takes 2 argument ,store, data"<<endl;
	  }
	}
	else if(commandArray[0] == FINDKEY){
	  if(numArgs == NUMFINDKEY){
	    //findKey stuff here
	    sendCommand = stringID + " " + myIP + " " + commandArray[0] + " " + commandArray[1];
	    foundCommand = true;
	  }
	  else{
	    cout<<"There is an incorrect number of arguments, find_key takes 2 argument, find_key, key"<<endl;
	  }
	}
	else if(commandArray[0] == EXIT){
	  if(numArgs == NUMEXIT){
	    cout<<"in exit"<<endl;
	    cout<<commandArray[0]<<endl;
	    sendCommand = stringID + " " + myIP +" " + commandArray[0];
	    foundCommand = true;
	    done = true;
	    //exit stuff here
	  }
	  else{
	    cout<<"There is an incorrect number of arguments, exit takes 1 arguments, exit"<<endl;
	  }
	}
	else if(commandArray[0] == SHUTDOWN){
	  if(numArgs == NUMSHUTDOWN){
	    sendCommand = stringID + " " + myIP +" " + commandArray[0];
	    foundCommand = true;
	    done = true;
	  }
	  else{
	    cout<<"There is an incorrect number of arguments, shutdown takes 1 arguments, shutdown"<<endl;
	  }
	}
	else if(commandArray[0] == SHOWKBUCKETS){
	  if(numArgs == NUMSHOWKBUCKETS){
	    foundCommand = true;
	    sendCommand = stringID + " " + myIP + " " + commandArray[0];
	  }
	  else{
	    cout<<"There is an incorrect number of arguments, showkbuckets takes 1 argument, showkbuckets"<<endl;
	  }
	}
	else{
	  cout<<"There is no command with that name" <<endl;
	}

	//*******************************************************************************
	if(foundCommand){
	  //send command to child process
	  /*
	  cout << "Sending message to child" << endl;
	  cout << "message: " << sendCommand.c_str() << endl;
	  cout << myIP << endl;
	  char ipBuf [MAXRECV];
	  
	  inet_ntop(AF_INET, &sendAddress.sin_addr, ipBuf,IPLEN);
	  //ASSERT:ipBuf holds the ipaddress
	  */
	  valid = inet_pton(AF_INET,myIP.c_str(),&sendAddress.sin_addr);
	  cout << "Message is " <<sendCommand << endl;
	  valid = sendto(sockfd, sendCommand.c_str(), sendCommand.size(),
			 MSG_NOSIGNAL, (struct sockaddr *)&sendAddress, sizeof(sendAddress));
	  
	}

	socklen_t sizeOfAddr;
	char buf[MAXRECV + 1];
	
	valid = recvfrom(sockfd,buf,MAXRECV,0,(struct sockaddr *) &recieveAddress,&sizeOfAddr);
	if(valid != -1){
	  //ASSERT:we have recieved a shutdown from our fork
	  done = true;
	}
	
      }
      cout<<"GoodBye From Main"<<endl;
    }
    
      
  }
  else{
    //ASSERT:we have an incorrect number of arguments and so should print an error
    cout<<"There is an incorrect number of arguments on the command line, the arguments should be as follows, myNodeID, friendNodeID,friendIPAddress,0 or 1, where 1 represents that this is the first node in the network and 0 represents that there are other nodes in the network. Please try again";
  }

 
  return 0;
}
