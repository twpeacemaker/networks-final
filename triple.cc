#include <sys/types.h>//fork
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include "triple.h"
#include <string>
#include <iostream>
#include "Constants.h"
using namespace std;
//CLASS CI: ipaddr is a defined string that exists as an IP
//address somewhere, port is a int that represents the port number of
//the ip address we are contacting for kademlia, nodeID represents the
//specific node that the IP address and port number are representing
//in kademlia

//Constructor
triple::triple(){}

//constructor
//PRE:none
//POST:creates an object with ipaddr=pIpaddr, nodeID=pNodeID and
//port=pPort
triple::triple(string pIpaddr, uint pNodeID, uint pPort){
  ipaddr = pIpaddr;
  nodeID = pNodeID;
  port = pPort;
}

//Copy Constructor
//PRE:Takes a triple as a parameter
//POST:makes this object a deep copy of aTriple
triple::triple(const triple & aTriple){
  ipaddr = aTriple.ipaddr;
  nodeID = aTriple.nodeID;
  port = aTriple.port;
}



//PRE:all member data is defined
//POST:returns the data in the triple as a string
string triple::getTriple() const{
  string returnVal;
  string stringNodeID = to_string(nodeID);
  string stringPort = to_string(port);

  returnVal = ipaddr + ' ' + stringPort + ' ' + stringNodeID;
  return returnVal;
  
}


//PRE:all member data is defined
//POST:returns the ip address of the triple
string triple::getIP() const{
  return ipaddr;
}

//PRE:all member data is defined
//POST:returns the port
uint triple::getPort() const{
  return port;
}

//PRE:all member data is defined
//POST:returns the nodeID
uint triple::getNodeID() const{
  return nodeID;
}

// PRE:  Takes a message
// POST: Sends message to node associated with this triple
void triple::sendMessage(string message, int & sockfd) {
  sockaddr_in sendAddress;
  memset(&sendAddress, 0 , sizeof(sendAddress));
  cout << "Sent: *"<< message << "* to " << ipaddr << endl;
  sendAddress.sin_family = AF_INET;
  sendAddress.sin_port = htons(FORKPORT);
  inet_pton(AF_INET, ipaddr.c_str(),&sendAddress.sin_addr);
  sendto(sockfd, message.c_str(), message.size(), MSG_NOSIGNAL,
		 (struct sockaddr *)&sendAddress, sizeof(sendAddress));
}

//PRE:both objects satisfy the CI
//POST:makes a deep copy of explicit equal to the implicit
triple & triple::operator = (const triple & aTriple){
  ipaddr = aTriple.ipaddr;
  nodeID = aTriple.nodeID;
  port = aTriple.port;
  return *this;
}

//PRE:takes a triple
//POST:returns true if given triple equals this triple and false otherwise
bool triple::operator == (const triple & aTriple) const {
  bool answer = true;
  if (ipaddr != aTriple.ipaddr) {
    answer = false;
  }
  if (nodeID != aTriple.nodeID) {
    answer = false;
  }
  if (port != aTriple.port) {
    answer = false;
  }
  return answer;
}
