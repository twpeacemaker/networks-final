#ifndef INCLUDED_CONSTANTS
#define INCLUDED_CONSTANTS


#define uint unsigned int
#define DELIMITER ' ' //if we want to change what we are using to
		      //delimit the strings

#define NEGATIVE '-'
#define BASE 10
#define ASCIIZERO (int) '0'
#define K 2
#define BITS 32
#define ALPHA 3
#define MAXWORDS 20 //represents the max words that will be in a
		   //message to another node
#define CORRECTNUMARGS 6
#define MAINPORT 12000
#define FORKPORT 13000
#define MAXRECV 500
#define IPLEN 15
#define COMMAND 2
#define MYID 1
#define MYIP 2
#define ARG5 5
#define FRIENDID 3
#define FRIENDIP 4

#define STORE "store"
#define ARGKEY 3 //the argument
#define NUMSTORE 2 //number of args any line with store at the front should have
#define BUFSIZE 50

#define SHOWKBUCKETS "showkbuckets"
#define NUMSHOWKBUCKETS 1

#define PING "ping"
#define NUMPING 2

#define WAITTIME 5 //number of seconds ping should wait
#define PINGBACK "pingback"


#define FINDKEY "find_key"
#define NUMFINDKEY 2 //number of args any line with find_key at front should have

#define EXIT "exit"
#define NUMEXIT 1

#define SHUTDOWN "shutdown"
#define NUMSHUTDOWN 1

#define EOS '\0'

#endif
